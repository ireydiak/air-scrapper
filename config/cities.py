AIRPORTS = {
    'Auckland': 'AKL',
    'Edinburgh': 'EDI',
    'Glasgow': 'GLA',
    'Málaga': 'MGP',
    'Montreal': 'YUL',
    'Oslo': 'OSL',
    'Reykjavik': 'REK',
    'Stockholm': 'STO',
    'Tokyo': 'TYO',
    'Vancouver': 'YVR'
}
