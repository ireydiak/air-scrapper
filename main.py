import os

import env
from time import strftime
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException, StaleElementReferenceException
from fare_aggregator import MomondoAggregator
from config import AIRPORTS
from multiprocessing import Process

cities_list = [AIRPORTS['Edinburgh'], AIRPORTS['Reykjavik'], AIRPORTS['Auckland']]
MIN_DAYS = 10
MAX_DAYS = 21


def page_scrap(destination: str):
    browser = webdriver.Firefox()
    start_date = '2019-08-14'
    end_date = '2019-08-22'
    momondo = MomondoAggregator(start_date, end_date, AIRPORTS['Montreal'], browser)
    flights = momondo.page_scrape(destination)

    try:
        filename = env.TARGET_FILENAME + '.{}.csv'.format(strftime("%Y%m%d-%H%M"))
        with open(env.TARGET_PATH + '/{}'.format(filename), 'a') as file:
            flights.to_csv(file, header=False, index=False)
            print('saved to {}'.format(filename))
            file.close()
    finally:
        browser.quit()


def directory_is_writable(path: str) -> bool:
    return os.path.exists(path) and os.path.isdir(path) and os.access(path, os.W_OK)


def setup():
    if not directory_is_writable(env.TARGET_PATH):
        raise FileNotFoundError
    pass


if __name__ == "__main__":
    try:
        setup()
        for city in cities_list:
            process = Process(target=page_scrap, args=(city,)).start()
    except FileNotFoundError as e:
        print('The path {} does not exist or is not writable'.format(env.TARGET_PATH))
    except StaleElementReferenceException as sere:
        print(sere.msg)
    except TimeoutException as te:
        print('Could not find the DOM elements in the view ', te.msg)
    except WebDriverException as wde:
        print(wde.msg)
