from abc import ABC, abstractmethod
from time import strftime

import pandas as pd
from selenium import webdriver


class FareAggregator(ABC):

    def __init__(self, start_date, end_date, from_airport, browser: webdriver):
        self.start_date = start_date
        self.end_date = end_date
        self.from_airport = from_airport
        self.browser = browser

    @property
    @abstractmethod
    def selector(self):
        """
        Excepts an AbstractSelector for the appropriate aggregator
        :return: selector.AbstractSelector
        """
        pass

    @property
    @abstractmethod
    def base_url(self) -> str:
        pass

    @property
    @abstractmethod
    def end_url(self) ->str:
        pass

    @property
    @abstractmethod
    def flight_search_url(self) -> str:
        pass

    @abstractmethod
    def fetch_prices(self) -> list:
        pass

    @abstractmethod
    def fetch_duration(self) -> [list, list]:
        pass

    @abstractmethod
    def fetch_times(self) -> [list, list]:
        pass

    @abstractmethod
    def fetch_carriers(self) -> [list, list]:
        pass

    @abstractmethod
    def fetch_dates(self) -> [list, list]:
        pass

    @abstractmethod
    def wait(self):
        pass

    def page_scrape(self, airport: str) -> pd.DataFrame:
        """
        :param airport: str
        :except selenium.common.exceptions.TimeoutException
        :return: pandas.DataFrame
        """
        print("[HTTP/GET] {}".format(self.full_url(airport)))
        self.browser.get(self.full_url(airport))

        # wait for the page to be asynchronously loaded
        # abstract method
        print("Starting scrape for {} ...".format(airport))
        self.wait()

        # carriers
        from_carriers_list, to_carriers_list = self.fetch_carriers()
        # dates
        from_dates_list, to_dates_list = self.fetch_dates()
        # times
        from_times_list, to_times_list = self.fetch_times()
        # duration
        from_duration_list, to_duration_list = self.fetch_duration()
        # prices
        prices_list = self.fetch_prices()

        flights = pd.DataFrame({
            'Out Date': from_dates_list,
            'Out Time': from_times_list,
            'Out Duration': from_duration_list,
            'Out Carrier': from_carriers_list,
            'Return Date': to_dates_list,
            'Return Time': to_times_list,
            'Return Duration': to_duration_list,
            'Return Carrier': to_carriers_list,
            'Price': prices_list
        })
        flights['Destination'] = airport
        flights['Timestamp'] = strftime('%Y-%m-%d @ %H:%M')

        print('Sending scrape data for {}'.format(airport))
        return flights.sort_values(by=['Price', 'Out Duration', 'Return Duration'])

    def full_url(self, to_airport):
        return '{}/{}/{}-{}/{}-flexible/{}-flexible?{}'.format(self.base_url, self.flight_search_url,
                                                               self.from_airport, to_airport, self.start_date,
                                                               self.end_date, self.end_url)
