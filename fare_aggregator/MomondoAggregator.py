from time import sleep

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By

from fare_aggregator.selector import AbstractSelector
from .FareAggregator import FareAggregator
from .selector.MomondoSelector import MomondoSelector
import re


class MomondoAggregator(FareAggregator):

    @property
    def selector(self) -> AbstractSelector:
        return MomondoSelector()

    @property
    def base_url(self) -> str:
        return 'https://www.momondo.ca'

    @property
    def end_url(self) -> str:
        return 'sort=price_a'

    @property
    def flight_search_url(self):
        return 'flight-search'

    def fetch_prices(self) -> list:
        """
        Fetch the prices on the browser using xpath
        :return: list
        """
        prices = self.browser.find_elements_by_xpath(self.selector.xp_prices)
        prices_list = [price.text.replace(' CAD', '').replace(',', '') for price in prices if price != '']
        return list(map(int, prices_list))

    def fetch_duration(self) -> [list, list]:
        """
        Fetch the duration for the inbound and outbound flights in the browser
        :return: list, list
        """
        raw_duration = self.browser.find_elements_by_xpath(self.selector.xp_duration)
        duration_list = [duration.text for duration in raw_duration]
        return duration_list[::2], duration_list[1::2]

    def fetch_times(self) -> [list, list]:
        """
        Fetch the times for the inbound and outbound flights in the browser
        :return: list, list
        """
        raw_times = self.browser.find_elements_by_xpath(self.selector.xp_times)
        times_list = [re.sub(r'/\+[0-9]+/', '', time.text) for time in raw_times]
        return times_list[::2], times_list[1::2]

    def fetch_carriers(self) -> [list, list]:
        """
        Fetch the carriers for the inbound and outbound flights in the browser
        :return: list, list
        """
        raw_carriers = self.browser.find_elements_by_xpath(self.selector.xp_carriers)
        carriers_list = [carrier.text for carrier in raw_carriers]
        return carriers_list[::2], carriers_list[1::2]

    def fetch_dates(self) -> [list, list]:
        """
        Fetch the departure dates and return dates in the browser
        :return: list, list
        """
        raw_dates = self.browser.find_elements_by_xpath(self.selector.xp_dates)
        dates_list = [date.text.replace('\n', ' ') for date in raw_dates]
        return dates_list[::2], dates_list[1::2]

    def wait(self):
        """
        The webpage is loading asynchronously
        Therefore we need to wait for a specific set of elements to be loaded before trying to acccess them
        :return: void
        """
        sleep(15)
        # WebDriverWait(self.browser, 15).until(
        #     expected_conditions.presence_of_all_elements_located(
        #         (By.XPATH, self.selector.xp_prices),
        #     )
        # )
