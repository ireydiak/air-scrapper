from .AbstractSelector import AbstractSelector


class MomondoSelector(AbstractSelector):
    @property
    def xp_dates(self):
        return '//div[@class="section date"]'

    @property
    def xp_carriers(self):
        return '//div[@class="section times"]/div[@class="bottom"]'

    @property
    def xp_times(self):
        return '//div[@class="section times"]/div[@class="top"]'

    @property
    def xp_duration(self):
        return '//div[@class="section duration"]/div[@class="top"]'

    @property
    def xp_prices(self):
        return '//div[@class="above-button"]//a[@class="booking-link "]/span[@class="price option-text"]'
