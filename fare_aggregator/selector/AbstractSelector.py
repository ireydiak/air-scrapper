from abc import ABC, abstractmethod


class AbstractSelector(ABC):

    @property
    @abstractmethod
    def xp_dates(self):
        pass

    @property
    @abstractmethod
    def xp_carriers(self):
        pass

    @property
    @abstractmethod
    def xp_times(self):
        pass

    @property
    @abstractmethod
    def xp_duration(self):
        pass

    @property
    @abstractmethod
    def xp_prices(self):
        pass
