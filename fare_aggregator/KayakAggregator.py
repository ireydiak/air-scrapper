from fare_aggregator.FareAggregator import FareAggregator


class KayakAggregator(FareAggregator):

    def __init__(self, start_date, end_date, from_airport):
        super().__init__(start_date, end_date, from_airport)

    @property
    def flight_search_url(self):
        return "flights"

    @property
    def end_url(self):
        return "sort=bestflight_a"

    @property
    def base_url(self):
        return "https://www.ca.kayak.com"
